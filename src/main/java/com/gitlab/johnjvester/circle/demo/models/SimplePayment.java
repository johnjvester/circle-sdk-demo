package com.gitlab.johnjvester.circle.demo.models;

import com.circle.client.model.CryptoPaymentsMoney;
import com.circle.client.model.PaymentIntentCreationRequest;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SimplePayment {
    private CryptoPaymentsMoney.CurrencyEnum currency;
    private String amount;
    @JsonProperty(value = "settlement_currency")
    private PaymentIntentCreationRequest.SettlementCurrencyEnum settlementCurrency;
}
