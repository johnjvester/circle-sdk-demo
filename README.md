# `circle-sdk-demo` Repository

[![pipeline status](https://gitlab.com/johnjvester/circle-sdk-demo/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/circle-sdk-demo/commits/master)


> The `circle-sdk-demo` repository contains a [Spring Boot](https://spring.io/projects/spring-boot) RESTful API service
> that acts as an integration service with the [Circle platform](https://www.circle.com/en/).

## Publications

This repository is related to a DZone.com publication:

* [(Spring) Booting Java to Accept Digital Payments with USDC](https://dzone.com/articles/spring-booting-java-to-accept-digital-payments-wit)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.