package com.gitlab.johnjvester.circle.demo.controllers;

import com.circle.client.ApiException;
import com.circle.client.model.ListBalancesResponse;
import com.gitlab.johnjvester.circle.demo.services.CircleIntegrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RequiredArgsConstructor
@RequestMapping(value = "/balances", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class BalancesController {
    private final CircleIntegrationService circleIntegrationService;
    @GetMapping
    public ResponseEntity<ListBalancesResponse> getBalances() {
        try {
            return new ResponseEntity<>(circleIntegrationService.getBalances(), HttpStatus.OK);
        } catch (ApiException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getResponseBody(), e);
        }
    }
}
