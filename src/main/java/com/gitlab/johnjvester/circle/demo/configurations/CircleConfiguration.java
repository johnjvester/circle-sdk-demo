package com.gitlab.johnjvester.circle.demo.configurations;

import com.circle.client.Circle;
import com.gitlab.johnjvester.circle.demo.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class CircleConfiguration {
    private final CircleConfigurationProperties circleConfigurationProperties;

    @Bean
    public Circle getCircle() {
        log.info("=======================");
        log.info("Initializing Circle SDK");
        log.info("=======================");

        log.info("basePath={}", Circle.SANDBOX_BASE_URL);
        log.info("circle.api-key={}",
                SecurityUtils.maskCredentialsRevealPrefix(
                        circleConfigurationProperties.getApiKey(), 7, '*'));

        Circle circle = Circle.getInstance()
                    .setBasePath(Circle.SANDBOX_BASE_URL)
                    .setApiKey(circleConfigurationProperties.getApiKey());
        log.info("circle={}", circle);

        log.info("==================================");
        log.info("Circle SDK Initialization Complete");
        log.info("==================================");

        return circle;
    }
}
