package com.gitlab.johnjvester.circle.demo.utils;

import org.apache.commons.lang3.StringUtils;

public final class SecurityUtils {
    private SecurityUtils() { }

    public static String maskCredentialsRevealPrefix(String string, int prefixLength, char maskCharacter) {
        return StringUtils.overlay(string, StringUtils.repeat(maskCharacter, string.length() - prefixLength), prefixLength, string.length());
    }

    public static String maskCredentialsRevealSuffix(String string, int suffixLength, char maskCharacter) {
        return StringUtils.overlay(string, StringUtils.repeat(maskCharacter, string.length() - suffixLength), 0, string.length() - suffixLength);
    }

}
