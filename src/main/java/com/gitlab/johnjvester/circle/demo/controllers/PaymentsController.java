package com.gitlab.johnjvester.circle.demo.controllers;

import com.circle.client.ApiException;
import com.circle.client.model.CreatePaymentIntentResponse;
import com.circle.client.model.GetPaymentIntentResponse;
import com.gitlab.johnjvester.circle.demo.models.SimplePayment;
import com.gitlab.johnjvester.circle.demo.services.CircleIntegrationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@RequiredArgsConstructor
@RequestMapping(value = "/payments", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class PaymentsController {
    private final CircleIntegrationService circleIntegrationService;

    @PostMapping
    public ResponseEntity<CreatePaymentIntentResponse> createPayment(@RequestBody SimplePayment simplePayment) {
        try {
            return new ResponseEntity<>(circleIntegrationService.createPayment(simplePayment), HttpStatus.CREATED);
        } catch (ApiException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getResponseBody(), e);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<GetPaymentIntentResponse> getPayment(@PathVariable("id") String id) {
        try {
            return new ResponseEntity<>(circleIntegrationService.getPayment(id), HttpStatus.OK);
        } catch (ApiException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getResponseBody(), e);
        }
    }
}
