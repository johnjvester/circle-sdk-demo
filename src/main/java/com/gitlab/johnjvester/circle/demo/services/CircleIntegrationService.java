package com.gitlab.johnjvester.circle.demo.services;

import com.circle.client.ApiException;
import com.circle.client.api.BalancesApi;
import com.circle.client.api.CryptoPaymentIntentsApi;
import com.circle.client.model.Chain;
import com.circle.client.model.CreatePaymentIntentRequest;
import com.circle.client.model.CreatePaymentIntentResponse;
import com.circle.client.model.CryptoPaymentsMoney;
import com.circle.client.model.GetPaymentIntentResponse;
import com.circle.client.model.ListBalancesResponse;
import com.circle.client.model.PaymentIntentCreationRequest;
import com.circle.client.model.PaymentMethodBlockchain;
import com.gitlab.johnjvester.circle.demo.models.SimplePayment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Slf4j
@Service
public class CircleIntegrationService {
    private final BalancesApi balancesApi = new BalancesApi();
    private final CryptoPaymentIntentsApi cryptoPaymentIntentsApi = new CryptoPaymentIntentsApi();

    public ListBalancesResponse getBalances() throws ApiException {
        ListBalancesResponse listBalancesResponse = balancesApi.listBalances();
        log.info("listBalancesResponse={}", listBalancesResponse);

        return listBalancesResponse;
    }

    public CreatePaymentIntentResponse createPayment(SimplePayment simplePayment) throws ApiException {
        CreatePaymentIntentRequest createPaymentIntentRequest = new CreatePaymentIntentRequest(new PaymentIntentCreationRequest()
                .idempotencyKey(UUID.randomUUID())
                .amount(
                        new CryptoPaymentsMoney()
                                .amount(simplePayment.getAmount())
                                .currency(simplePayment.getCurrency())
                )
                .settlementCurrency(simplePayment.getSettlementCurrency())
                .paymentMethods(
                        Collections.singletonList(
                                new PaymentMethodBlockchain()
                                        .chain(Chain.ETH)
                                        .type(PaymentMethodBlockchain.TypeEnum.BLOCKCHAIN)
                        )
                ));

        CreatePaymentIntentResponse createPaymentIntentResponse = cryptoPaymentIntentsApi.createPaymentIntent(createPaymentIntentRequest);
        log.info("createPaymentIntentResponse={}", createPaymentIntentResponse);

        return createPaymentIntentResponse;
    }

    public GetPaymentIntentResponse getPayment(String id) throws ApiException {
        UUID paymentIntentId = UUID.fromString(id);
        log.info("paymentIntentId={} from id={}", paymentIntentId, id);
        GetPaymentIntentResponse getPaymentIntentResponse = cryptoPaymentIntentsApi.getPaymentIntent(paymentIntentId);
        log.info("getPaymentIntentResponse={}", getPaymentIntentResponse);

        return getPaymentIntentResponse;
    }
}
