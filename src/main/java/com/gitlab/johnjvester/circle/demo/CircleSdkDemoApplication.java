package com.gitlab.johnjvester.circle.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircleSdkDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CircleSdkDemoApplication.class, args);
    }

}
